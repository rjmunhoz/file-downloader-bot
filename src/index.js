import dotenv from 'dotenv-safe';
import TelegramAPI from 'tg-cli-node';
import {log, error} from './lib/utils/log';
import commands from './lib/commands';
dotenv.load();

const Client = new TelegramAPI({
    telegram_cli_path: process.env.TELEGRAM_CLI_PATH,
    telegram_cli_socket_path: process.env.TELEGRAM_CLI_SOCKET_PATH,
    server_publickey_path: process.env.SERVER_PUBLICKEY_PATH
});

Client.connect(connection => {
    connection.on('message', message => {
        log(message.text, 'New message');

        let found = false;

        for (const command in commands) {
            if (command.regex.test(message.text)) {
                found = true;
                command.run(message)
                .then(response => {
                    message.reply(response.text);
                });
                break;
            }
        }

        if (!found) {
            message.send('Comando não encontrado; digite /help para obetr uma lista de comandos disponíveis');
        }

    });

    connection.on('error', e => {
        error(e, 'Error from Telegram API');
    });

    connection.on('disconnect', () => {
        log('Disconnected from Telegram API');
    });
});


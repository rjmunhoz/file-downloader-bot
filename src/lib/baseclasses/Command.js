export default class Command {

    constructor() {
        this.name = '';
        this.regex = new RegExp();
        this.helpText = '';
        this.showInHelp = false;
    }

    static run(msg) {
        return new Promise(res => res({text: msg.text}));
    }

}

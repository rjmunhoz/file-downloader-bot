import fs from 'fs';
import path from 'path';

const commandsDir = path.join(__dirname, 'commands');

const commands = {};

fs.readdirSync(
    path.join(commandsDir)).forEach(el => {
        const moduleName = el.split('.')[0];
        const module = require(path.join(commandsDir, moduleName));
        commands[moduleName] = new module.default();
    }
);

export default commands;
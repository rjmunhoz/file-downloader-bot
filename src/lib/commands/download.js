import fs from 'fs';
import path from 'path';
import request from 'request';
import progress from 'request-progress';
import Command from '../baseclasses/Command';

export default class Download extends Command {
    constructor() {
        super();
        this.name = 'download';
        this.regex = /\/download (https?:\/\/.*)/;
        this.helpText = 'Baixa um arquivo do link especificado. (Somente links diretos)';
        this.showInHelp = true;
    }

    run(msg) {
        return new Promise((res, rej) => {
            if (msg.text.match(this.regex)[1]) {
                msg.reply('Downloading. Please, wait');
                progress(request(msg.text.match(this.regex)[1]))
                .on('progress', state => {
                    msg.reply(`Downloading: ${state.percent * 100}%`);
                })
                .on('error', rej)
                .on('end', () => {
                    res({
                        text: 'Done downloading!'
                    });
                })
                .pipe(
                    fs.createWriteStream(
                        path.join(process.env.DOWNLOADS_LOCATION, msg.id)
                    )
                );
            } else {
                rej({
                    text: 'Você não informou uma URL válida'
                });
            }
        });
    }

}
import Command from '../baseclasses/Command';

export default class Echo extends Command {
    constructor() {
        super();
        this.name = 'echo';
        this.regex = /\/echo (.*)/;
        this.helpText = 'Responde com o texto da mensagem enviada';
        this.showInHelp = true;
    }

    run(msg) {
        return new Promise(res => {
            res({text: msg.match(this.regex)[1] || 'Digite um texto!'});
        });
    }
}

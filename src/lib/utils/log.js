/* eslint-disable no-console */
import chalk from 'chalk';
import pj from 'prettyjson';

const log = (msg, prefix) => console.log(chalk.cyan(prefix ? `${prefix}: ${pj.render(msg)}` : pj.render(msg)));

const error = (msg, prefix) => console.error(chalk.red(prefix ? `${prefix}: ${pj.render(msg)}` : pj.render(msg)));

const info = (msg, prefix) => console.info(chalk.blue(prefix ? `${prefix}: ${pj.render(msg)}` : pj.render(msg)));

const warn = (msg, prefix) => console.warn(chalk.yellow(prefix ? `${prefix}: ${pj.render(msg)}` : pj.render(msg)));

export {
    log,
    error,
    info,
    warn
};